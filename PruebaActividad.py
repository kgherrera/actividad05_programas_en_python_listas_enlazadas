'''
Created on 20/10/2021

@author: Herrera
'''
import datetime

class Temperaturas:
    def __init__(self, temperatura, fechaRegistro):
        self.temperatura = temperatura
        self.fechaRegistro = fechaRegistro
    
    def __str__(self):
        return f"Temperatura: {self.temperatura}, Fecha de registro: {self.fechaRegistro}"
        
class Nodo:
    def __init__(self, dato):
        self.dato = dato
        self.nodoSiguiente = None
    
    def __str__(self):
        return str(self.value)
    
class ListaEnlazada:
    def __init__(self):
        self.nodoInicio = None
        self.nodoFin = None
        
    def verificarListaVacia(self):
        if (self.nodoInicio == None):
            return True
        else:
            return False
    
    def agregarElementoInicio(self, dato):
        nodoNuevo = Nodo(dato)
        if(self.nodoInicio == None):
            self.nodoInicio = self.nodoFin = nodoNuevo
            print("\nSe agrego el dato")
        else:
            nodoNuevo.nodoSiguiente = self.nodoInicio
            self.nodoInicio = nodoNuevo
            print("\nSe agrego el dato")
    
    def agregarAlFinal(self, dato):
        nodoNuevo = Nodo(dato)
        if(self.nodoInicio == None):
            self.nodoInicio = self.nodoFin = nodoNuevo
            print("Se agrego el dato")
        else:
            self.nodoFin.nodoSiguiente = nodoNuevo
            self.nodoFin = nodoNuevo
            print("Se agrego el dato")
    
    def agregarElementoPosicionEspecifica(self, posicion, dato):
        nodoNuevo = Nodo(dato)
        if(posicion == 0):
            ListaEnlazada.agregarElementoInicio(self, dato)
        elif(self.nodoInicio == None):
            print("\nNo se encontro la pocicion")
        else:
            nodoActual = self.nodoInicio.nodoSiguiente
            nodoAnterior = self.nodoInicio
            cont = 1
            
            while(nodoActual != None and posicion != cont):
                cont+=1
                nodoActual = nodoActual.nodoSiguiente
                nodoAnterior = nodoAnterior.nodoSiguiente
            if(cont == posicion):
                nodoAnterior.nodoSiguiente = nodoNuevo
                nodoNuevo.nodoSiguiente = nodoActual   
                print("\nSe agrego el dato")
            else:
                print("\nNo se encontro la pocicion")
            
        
    def eliminarInicio(self):
        if(self.nodoInicio == None):
            print("\nLista vacia")
        else:
            if(self.nodoInicio == self.nodoFin):
                self.nodoFin = self.nodoInicio = None
                print("\nSe elimino el dato")
            else:
                self.nodoInicio = self.nodoInicio.nodoSiguiente
                print("\nSe elimino el dato")
    
    def eliminarFinal(self):
        if(self.nodoInicio == None):
            print("\nLista vacia")
        else:
            if(self.nodoInicio == self.nodoFin):
                self.nodoInicio = self.nodoFin = None
                print("\nSe elimino el dato")
            else:
                nodoActual = self.nodoInicio
                nodoAnterior = None
                
                while(nodoActual != self.nodoFin):
                    nodoAnterior = nodoActual
                    nodoActual = nodoActual.nodoSiguiente
                
                nodoAnterior.nodoSiguiente = None
                self.nodoFin = nodoAnterior
                print("\nSe elimino el dato")
                
    def eliminarElementoPosicionEspecifica(self, posicion):
        if(self.nodoInicio == None):
            print("\nLista vacia")
        elif (posicion == 0):
            ListaEnlazada.eliminarInicio(self)
        else:
            cont = 1
            nodoActual = self.nodoInicio.nodoSiguiente
            nodoAnterior = self.nodoInicio
            while(nodoActual.nodoSiguiente != None and cont != posicion):
                cont += 1
                nodoAnterior = nodoAnterior.nodoSiguiente
                nodoActual = nodoActual.nodoSiguiente 
            if(cont == posicion):
                nodoAnterior.nodoSiguiente = nodoActual.nodoSiguiente
                print("\nDato eliminado");
            else:
                print("\nNo se encontro la posicion");
    
    def eliminarEspecifico(self, dato):
        if(self.nodoInicio == None):
            print("\nLista vacia")
        elif (self.nodoInicio.dato == dato):
            ListaEnlazada.eliminarInicio(self)
        else:
            nodoAnterior = self.nodoInicio
            nodoActual = self.nodoInicio.nodoSiguiente
            while(dato != nodoActual.dato and nodoActual.nodoSiguiente != None):
                nodoAnterior = nodoAnterior.nodoSiguiente
                nodoActual = nodoActual.nodoSiguiente
            
            if(nodoActual.dato == dato):
                nodoAnterior.nodoSiguiente = nodoActual.nodoSiguiente
                print("\nSe elimino el dato")
            else:
                print("\nNo se encontro el dato")
            
    def mostrarCantidadElementos(self):
        cont = 0
        nodoActual = self.nodoInicio
        
        while(nodoActual != None):
            cont += 1
            nodoActual = nodoActual.nodoSiguiente
            
        print(f"\nCantidad de elementos: {cont}")
        
    def mostrarListaEnlazada(self):
        nodoActual = self.nodoInicio
        print()
        while(nodoActual != None):
            print(f"[{nodoActual.dato}] ")
            nodoActual = nodoActual.nodoSiguiente  
        
    
    def mostrarPromedioTemperaturas(self):
        if(self.nodoInicio == None):
            print("Lista vacia")
        else:
            sumaTemp = 0
            nodoActual = self.nodoInicio
            cont = 0
            while(nodoActual != None):
                
                sumaTemp = sumaTemp + nodoActual.dato.temperatura
                nodoActual = nodoActual.nodoSiguiente
                cont+=1
            
            sumaTemp = sumaTemp/cont
            print("\nPromedio de temperaturas: ")
            if(sumaTemp >= 0 and sumaTemp <= 10):
                print(f"{sumaTemp} --> congelacion")
            elif(sumaTemp >= 11 and sumaTemp <= 20):
                print(f"{sumaTemp} --> frio")
            elif(sumaTemp >= 21 and sumaTemp <= 30):
                print(f"{sumaTemp} --> normal")
            elif(sumaTemp >= 31):
                print(f"{sumaTemp} --> alta")
    
    def mostrarPromedioTemperaturasRango(self, ran1, ran2):
        if(self.nodoInicio == None):
            print("Lista vacia")
        else:
            r1 = ran1.split("/")
            r2 = ran2.split("/")
            
            rango1 = datetime.date(int(r1[2]), int(r1[1]), int(r1[0]))
            rango2 = datetime.date(int(r2[2]), int(r2[1]), int(r2[0]))
            
            nodoActual = self.nodoInicio
            sumaTemp = 0
            cont = 0
            while(nodoActual != None):
                vF = nodoActual.dato.fechaRegistro.split("/")
                fA = datetime.date(int(vF[2]), int(vF[1]), int(vF[0]))
                
                if(fA >= rango1 and fA <= rango2):
                    sumaTemp = sumaTemp + nodoActual.dato.temperatura
                    cont+=1
                nodoActual = nodoActual.nodoSiguiente
            
            print(f"\nPromedio de temperaturas entre {ran1} - {ran2}")
            sumaTemp = sumaTemp/cont
            if(sumaTemp >= 0 and sumaTemp <= 10):
                print(f"{sumaTemp} --> congelacion")
            elif(sumaTemp >= 11 and sumaTemp <= 20):
                print(f"{sumaTemp} --> frio")
            elif(sumaTemp >= 21 and sumaTemp <= 30):
                print(f"{sumaTemp} --> normal")
            elif(sumaTemp >= 31):
                print(f"{sumaTemp} --> alta")     
        
le1 = ListaEnlazada()


opcion = 0
le1.agregarAlFinal(Temperaturas(18.1, "12/10/2003"))
le1.agregarAlFinal(Temperaturas(15.3, "12/10/2005"))
le1.agregarAlFinal(Temperaturas(29.9, "12/10/2002"))
le1.agregarAlFinal(Temperaturas(28.8, "12/11/2006"))
le1.agregarAlFinal(Temperaturas(20.9, "12/08/2010"))

while(opcion != 11):
    print("\nElige opcion")
    print("1) Agregar elemento inicio")
    print("2) Agregar elemento Final")
    print("3) Eliminar elemento inicio")
    print("4) Eliminar elemento final")
    print("5) Mostrar Temperaturas")
    print("6) Mostrar cantidad de elementos")
    print("7) eliminar elemento en posicion especifica")
    print("8) agregar elemento en posicion especifica")
    print("9) mostrar promedio de temperaturas")
    print("10) mostrar promedio de temperaturas en rango especifico")
    print("11) Salir")
    opcion = int(input("Introduce opcion: "))
    
    if(opcion == 1):
        temperatura = int(input("Introduce temperatura: "))
        fechaRegistro = input("Introduce fecha registro: ")
        le1.agregarElementoInicio(Temperaturas(temperatura, fechaRegistro))
        
    elif(opcion == 2):
        temperatura = int(input("Introduce temperatura: "))
        fechaRegistro = input("Introduce fecha registro: ")
        le1.agregarAlFinal(Temperaturas(temperatura, fechaRegistro))
    elif(opcion == 3):
        le1.eliminarInicio()
    elif(opcion == 4):
        le1.eliminarFinal()
    elif(opcion == 5):
        le1.mostrarListaEnlazada()
    elif(opcion == 6):
        le1.mostrarCantidadElementos()
    elif(opcion == 7):
        posicion = int(input("Introduce posicion: "))
        le1.eliminarElementoPosicionEspecifica(posicion)
    elif(opcion == 8):
        posicion = int(input("Introduce posicion: "))
        temperatura = int(input("Introduce temperatura: "))
        fechaRegistro = input("Introduce fecha registro formato(dd/mm/aaaa): ")
        le1.agregarElementoPosicionEspecifica(posicion, Temperaturas(temperatura, fechaRegistro))
    elif(opcion == 9):
        le1.mostrarPromedioTemperaturas()
    elif(opcion == 10):
        rango1 = input("Introduce fecha inferior formato(dd/mm/aaaa): ")
        rango2 = input("Introduce fecha superior mismo formato: ")
        le1.mostrarPromedioTemperaturasRango(rango1, rango2)
    elif(opcion == 11):
        print("Saliendo , . .")